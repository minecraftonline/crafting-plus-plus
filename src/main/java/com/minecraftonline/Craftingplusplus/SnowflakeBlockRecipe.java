package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class SnowflakeBlockRecipe extends TwoByTwoRecipe implements CraftingRecipe {

    SnowflakeBlockRecipe()
    {
        SnowflakeInfo.checkInitialized();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return SnowflakeInfo.snowflakeBlockItem;
    }

    @Override
    public String getId() {
        return "snowflake_block";
    }

    @Override
    public String getName() {
        return "snowflake block";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        return isTwoByTwo(grid);
    }

    protected boolean hasRequiredItem(Optional<Slot> slot)
    {
        if (!slot.isPresent()) return false;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return false;

        ItemStack item = slotItem.get();

        if (!item.getType().equals(SnowflakeInfo.snowflakeItem.getType())) return false;
        Optional<Text> name = item.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(SnowflakeInfo.snowflakeName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        Optional<ItemStack> slotItem = CraftingHelper.getFirstItem(grid);
        if (!slotItem.isPresent()) return SnowflakeInfo.snowflakeBlockItem;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(SnowflakeInfo.snowflakeBlockItem.getType())
                .add(Keys.DISPLAY_NAME, SnowflakeInfo.snowflakeBlockName);

        customItem = CraftingHelper.copyExtras(ingredient, customItem);

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
