package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.Optional;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class VinesHelmetRecipe extends SpookyInfo implements CraftingRecipe {
    private ItemStackSnapshot helmetItem;
    private Text helmetName = Text.of(TextColors.GREEN, "Crown of Vines");
    {
        helmetItem = ItemStack.builder()
                .itemType(ItemTypes.CHAINMAIL_HELMET)
                .add(Keys.DISPLAY_NAME, helmetName)
                .add(Keys.ITEM_LORE, vineLore)
                .add(Keys.ITEM_ENCHANTMENTS, vineEnchants)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return helmetItem;
    }

    @Override
    public String getId() {
        return "vines_helmet";
    }

    @Override
    public String getName() {
        return "vines helmet";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        return isValidWithOffset(grid, world, 0) || isValidWithOffset(grid, world, 1);
    }
    
    private boolean isValidWithOffset(CraftingGridInventory grid, World world, int offset) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (y <= (1 + offset) && y >= (0 + offset) && !(y == (1 + offset) && x == 1))
                {
                    // top two rows besides middle
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    if (y == (0 + offset) && x == 1)
                    {
                        // top middle
                        if (!ingredient.getType().equals(ItemTypes.PUMPKIN_PIE)) return false;
                        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                        if (!name.isPresent() || !name.get().equals(pumpkinPieName)) return false;
                    }
                    else
                    {
                        if (!ingredient.getType().equals(ItemTypes.VINE)) return false;
                    }
                }
                else
                {
                    // empty spot (middle top)
                    if (slotItem.isPresent()) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return helmetItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
