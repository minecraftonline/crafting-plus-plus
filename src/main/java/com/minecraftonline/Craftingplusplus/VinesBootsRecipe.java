package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.Optional;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class VinesBootsRecipe extends SpookyInfo implements CraftingRecipe {
    private ItemStackSnapshot bootsItem;
    private Text bootsName = Text.of(TextColors.GREEN, "Aglet of Vines");
    {
        bootsItem = ItemStack.builder()
                .itemType(ItemTypes.CHAINMAIL_BOOTS)
                .add(Keys.DISPLAY_NAME, bootsName)
                .add(Keys.ITEM_LORE, vineLore)
                .add(Keys.ITEM_ENCHANTMENTS, vineEnchants)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return bootsItem;
    }

    @Override
    public String getId() {
        return "vines_boots";
    }

    @Override
    public String getName() {
        return "vines boots";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        return isValidWithOffset(grid, world, 0) || isValidWithOffset(grid, world, 1);
    }
    
    private boolean isValidWithOffset(CraftingGridInventory grid, World world, int offset) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (x != 1 && y >= (0 + offset) && y <= (1 + offset))
                {
                    // two rows besides middle column
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    if (y == (1 + offset))
                    {
                        // bottom row
                        if (!ingredient.getType().equals(ItemTypes.PUMPKIN_PIE)) return false;
                        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                        if (!name.isPresent() || !name.get().equals(pumpkinPieName)) return false;
                    }
                    else
                    {
                        if (!ingredient.getType().equals(ItemTypes.VINE)) return false;
                    }
                }
                else
                {
                    // empty spot
                    if (slotItem.isPresent()) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return bootsItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
