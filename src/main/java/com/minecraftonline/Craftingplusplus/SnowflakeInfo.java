package com.minecraftonline.Craftingplusplus;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SnowflakeInfo {

    public static ItemStackSnapshot snowflakeItem;
    public static ItemStackSnapshot snowflakeItemFromBlock;
    public static ItemStackSnapshot snowflakeBlockItem;
    public static ItemStackSnapshot snowflakeCrystalItem;
    public static Text snowflakeName = Text.of(TextColors.DARK_AQUA, "Snowflake");
    public static Text snowflakeBlockName = Text.of(TextColors.DARK_AQUA, "Snowflake Block");
    public static Text snowflakeCrystalName = Text.of(TextColors.DARK_AQUA, "Snowflake Crystal");
    public static int snowflakeQuantityPerBlock = 4;

    public SnowflakeInfo()
    {
        checkInitialized();
    }

    private static boolean initialized = false;

    public static void initialize() {
        // these have to be created later so that the builders are defined
        snowflakeItem = ItemStack.builder()
                .itemType(ItemTypes.SNOWBALL)
                .add(Keys.DISPLAY_NAME, snowflakeName)
                .build().createSnapshot();
        ItemStack snowflakeFromBlock = ItemStack.builder()
                .itemType(ItemTypes.SNOWBALL)
                .add(Keys.DISPLAY_NAME, snowflakeName)
                .build();
        snowflakeFromBlock.setQuantity(snowflakeQuantityPerBlock);
        snowflakeItemFromBlock = snowflakeFromBlock.createSnapshot();
        snowflakeBlockItem = ItemStack.builder()
                .itemType(ItemTypes.SNOW)
                .add(Keys.DISPLAY_NAME, snowflakeBlockName)
                .build().createSnapshot();
        snowflakeCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, snowflakeCrystalName)
                .build().createSnapshot();

        initialized = true;
    }

    public static void checkInitialized() {
        if (!initialized)
        {
            initialize();
        }
    }

}
