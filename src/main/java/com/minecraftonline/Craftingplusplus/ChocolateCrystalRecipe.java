package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.minecraftonline.Craftingplusplus.ChocolateIngotFromNuggetsRecipe;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.GoldenApple;
import org.spongepowered.api.data.type.GoldenApples;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class ChocolateCrystalRecipe implements CraftingRecipe {
    private ItemStackSnapshot chocolateEggItem;
    private ItemStackSnapshot chocolateCrystalItem;
    private Text chocolateEggName = Text.of(TextColors.DARK_PURPLE, "Golden Cadbunny egg");
    private Text chocolateCrystalName = Text.of(TextColors.GOLD, "Chocolate Crystal");
    {
        chocolateEggItem = ItemStack.builder()
                .itemType(ItemTypes.GOLDEN_APPLE)
                .add(Keys.GOLDEN_APPLE_TYPE, GoldenApples.ENCHANTED_GOLDEN_APPLE)
                .add(Keys.DISPLAY_NAME, chocolateEggName)
                .build().createSnapshot();
        chocolateCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, chocolateCrystalName)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return chocolateCrystalItem;
    }

    @Override
    public String getId() {
        return "chocolate_crystal";
    }

    @Override
    public String getName() {
        return "chocolate crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    if (!ingredient.getType().equals(chocolateEggItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(chocolateEggName)) return false;
                    Optional<GoldenApple> appleType = ingredient.get(Keys.GOLDEN_APPLE_TYPE);
                    if (!appleType.isPresent() || !appleType.get().equals(GoldenApples.ENCHANTED_GOLDEN_APPLE)) return false;
                }
                else
                {
                    // outside ingredient
                    if (!(ChocolateIngotFromNuggetsRecipe.isChocolateNuggetItem(ingredient))) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        // get middle slot lore item
        Optional<Slot> slot = grid.getSlot(1, 1);
        if (!slot.isPresent()) return chocolateCrystalItem;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return chocolateCrystalItem;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(chocolateCrystalItem.getType())
                .add(Keys.DISPLAY_NAME, chocolateCrystalName);

        // copy lore if it exists
        Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
        if (lore.isPresent())
        {
            // second line has event name
            if (lore.get().size() >= 2)
            {
                List<Text> customLore = Arrays.asList(lore.get().get(1));
                customItem.add(Keys.ITEM_LORE, customLore);
            }
        }

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
