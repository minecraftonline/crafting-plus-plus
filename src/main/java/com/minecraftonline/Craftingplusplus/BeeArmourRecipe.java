package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import com.google.common.collect.Lists;
import java.util.Optional;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.world.World;


public class BeeArmourRecipe extends HoneyInfo implements CraftingRecipe {

    private static final List<ItemType> ARMOURTYPES = Lists.newArrayList(ItemTypes.LEATHER_HELMET,
            ItemTypes.LEATHER_CHESTPLATE, ItemTypes.LEATHER_LEGGINGS, ItemTypes.LEATHER_BOOTS);
    private static final List<Integer> ARMOURDURABILITY = Lists.newArrayList(55, 80, 75, 65);
    private static final List<String> ARMOURNAMES = Lists.newArrayList("Helmet",
            "Chestplate", "Leggings", "Boots");

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return armourItem;
    }

    @Override
    public String getId() {
        return "bee_armour";
    }

    @Override
    public String getName() {
        return "bee armour";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        Optional<Text> honeyIngredientName = null;
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient has to be a leather armour piece
                    if (!ARMOURTYPES.contains(ingredient.getType())) return false;
                    // only works on normal leather armour
                    Optional<List<Text>> itemLore = ingredient.get(Keys.ITEM_LORE);
                    boolean isBeeArmour = false;
                    if (itemLore.isPresent()) {
                        isBeeArmour = itemLore.get().equals(armourLore)
                                   || itemLore.get().equals(nbArmourLore);
                        // only allow use on armour that is already bee armour
                        if (!isBeeArmour) return false;
                    } else {
                        isBeeArmour = false;
                    }
                    if (honeyIngredientName.get().equals(nbHoneyName)) {
                        // Nonbinary honey can only be used on bee armour
                        if (!isBeeArmour) return false;
                    }
                }
                else
                {
                    // outside ingredient
                    if (!ingredient.getType().equals(honeyItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent()) return false;

                    if (!name.get().equals(honeyName) && !name.get().equals(nbHoneyName)) return false;

                    // all the honey types are the same
                    if (honeyIngredientName == null) {
                        honeyIngredientName = name;
                    } else if (!honeyIngredientName.equals(name)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        // get middle slot lore item
        Optional<Slot> slot = grid.getSlot(1, 1);
        if (!slot.isPresent()) return armourItem;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return armourItem;

        // get first slot for honey type
        Optional<Slot> firstSlot = grid.getSlot(0, 0);
        if (!firstSlot.isPresent()) return armourItem;

        Optional<ItemStack> firstSlotItem = firstSlot.get().peek();
        if (!firstSlotItem.isPresent()) return armourItem;

        ItemStack armourPiece = slotItem.get();
        if (!ARMOURTYPES.contains(armourPiece.getType())) return armourItem;
        int index = ARMOURTYPES.indexOf(armourPiece.getType());

        Optional<Text> optFirstItemName = firstSlotItem.get().get(Keys.DISPLAY_NAME);
        if (!optFirstItemName.isPresent()) return armourItem;
        Text firstItemName = optFirstItemName.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder().from(armourPiece.copy())
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Bee ", ARMOURNAMES.get(index)));

        if (!firstItemName.equals(nbHoneyName)) {
            // only regular honey applies colour
            // direct copy
            customItem.add(Keys.COLOR, Color.ofRgb(16763904))
                      .add(Keys.ITEM_LORE, armourLore);
        } else {
            // Nonbinary honey, repair and apply unbreaking 3 enchant

            Optional<List<Enchantment>> optEnchants = armourPiece.get(Keys.ITEM_ENCHANTMENTS);
            List<Enchantment> enchants;
            Enchantment unbreaking3 = Enchantment.of(EnchantmentTypes.UNBREAKING, 3);
            if (optEnchants.isPresent()) {
                enchants = optEnchants.get();
                enchants.add(unbreaking3);
            } else {
                enchants = Lists.newArrayList(unbreaking3);
            }
            customItem.add(Keys.ITEM_ENCHANTMENTS, enchants)
                      .add(Keys.ITEM_DURABILITY, ARMOURDURABILITY.get(index))
                      .add(Keys.ITEM_LORE, nbArmourLore);
        }

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
