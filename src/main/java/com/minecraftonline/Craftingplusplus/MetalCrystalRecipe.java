package com.minecraftonline.Craftingplusplus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class MetalCrystalRecipe implements CraftingRecipe {
    private ItemStackSnapshot lensItem;
    private ItemStackSnapshot laserEyeItem;
    private ItemStackSnapshot metalItem;
    private ItemStackSnapshot metalCrystalItem;
    private Text lensName = Text.of(TextColors.RED, "Focusing Lens");
    private Text laserEyeName = Text.of(TextColors.GREEN, "Laser eye");
    private Text metalName = Text.of(TextColors.DARK_GRAY, "Scrap Metal");
    private Text metalCrystalName = Text.of(TextColors.GRAY, "Metal Crystal");
    {
        lensItem = ItemStack.builder()
                .itemType(ItemTypes.STAINED_GLASS_PANE)
                .add(Keys.DYE_COLOR, DyeColors.RED)
                .add(Keys.DISPLAY_NAME, lensName)
                .build().createSnapshot();
        laserEyeItem = ItemStack.builder()
                .itemType(ItemTypes.ENDER_EYE)
                .add(Keys.DISPLAY_NAME, laserEyeName)
                .build().createSnapshot();
        metalItem = ItemStack.builder()
                .itemType(ItemTypes.IRON_INGOT)
                .add(Keys.DISPLAY_NAME, metalName)
                .build().createSnapshot();
        metalCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, metalCrystalName)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return metalCrystalItem;
    }

    @Override
    public String getId() {
        return "metal_crystal";
    }

    @Override
    public String getName() {
        return "metal crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient, could be focusing lens or laser eye
                    if (!(isFocusingLens(ingredient) || isLaserEye(ingredient))) return false;
                }
                else
                {
                    // outside ingredient
                    if (!ingredient.getType().equals(metalItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(metalName)) return false;
                }
            }
        }

        return true;
    }

    protected boolean isFocusingLens(ItemStack ingredient) {
        if (!ingredient.getType().equals(lensItem.getType())) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(lensName)) return false;

        return true;
    }

    protected boolean isLaserEye(ItemStack ingredient) {
        if (!ingredient.getType().equals(laserEyeItem.getType())) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(laserEyeName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return metalCrystalItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        List<ItemStackSnapshot> remaining = new ArrayList<ItemStackSnapshot>();
        for (int i = 0; i < grid.capacity(); i++)
        {
            if (i != 4) remaining.add(ItemStackSnapshot.NONE);
            else
            {
                ItemStack middleItem = grid.getSlot(new SlotIndex(4)).get().peek().get();
                // consume laser eye, but don't consume focusing lens
                if (isFocusingLens(middleItem)) remaining.add(4, middleItem.createSnapshot());
                else remaining.add(4, ItemStackSnapshot.NONE);
            }
        }
        return remaining;
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
