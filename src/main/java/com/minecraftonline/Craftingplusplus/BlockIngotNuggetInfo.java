package com.minecraftonline.Craftingplusplus;

import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;

public abstract class BlockIngotNuggetInfo {

    public ItemStackSnapshot nuggetItem;
    public ItemStackSnapshot nuggetItemFromIngot;
    public ItemStackSnapshot ingotItem;
    public ItemStackSnapshot ingotItemFromBlock;
    public ItemStackSnapshot blockItem;
    public Text nuggetName;
    public Text ingotName;
    public Text blockName;
    public List<Text> nuggetLore;
    public List<Text> ingotLore;
    public List<Text> blockLore;
    private static int quantity = 9;

    public void initialise()
    {
        nuggetItem = ItemStack.builder()
                .itemType(ItemTypes.GOLD_NUGGET)
                .add(Keys.DISPLAY_NAME, nuggetName)
                .add(Keys.ITEM_LORE, nuggetLore)
                .build().createSnapshot();
        ItemStack nuggetFromIngot = ItemStack.builder()
                .itemType(ItemTypes.GOLD_NUGGET)
                .add(Keys.DISPLAY_NAME, nuggetName)
                .add(Keys.ITEM_LORE, nuggetLore)
                .build();
        nuggetFromIngot.setQuantity(quantity);
        nuggetItemFromIngot = nuggetFromIngot.createSnapshot();
        ingotItem = ItemStack.builder()
                .itemType(ItemTypes.GOLD_INGOT)
                .add(Keys.DISPLAY_NAME, ingotName)
                .add(Keys.ITEM_LORE, ingotLore)
                .build().createSnapshot();
        ItemStack ingotFromBlock = ItemStack.builder()
                .itemType(ItemTypes.GOLD_INGOT)
                .add(Keys.DISPLAY_NAME, ingotName)
                .add(Keys.ITEM_LORE, ingotLore)
                .build();
        ingotFromBlock.setQuantity(quantity);
        ingotItemFromBlock = ingotFromBlock.createSnapshot();
        blockItem = ItemStack.builder()
                .itemType(ItemTypes.GOLD_BLOCK)
                .add(Keys.DISPLAY_NAME, blockName)
                .add(Keys.ITEM_LORE, blockLore)
                .build().createSnapshot();
    }

}
