package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DirtType;
import org.spongepowered.api.data.type.DirtTypes;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class TerrainCrystalRecipe implements CraftingRecipe {
    private ItemStackSnapshot compactItem;
    private ItemStackSnapshot dirtItem;
    private ItemStackSnapshot terrainCrystalItem;
    private Text compactTerrainName = Text.of(TextColors.GOLD, "Compact Terrain");
    private Text terrainCrystalName = Text.of(TextColors.GOLD, "Terrain Crystal");
    {
        compactItem = ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.SMOOTH_GRANITE)
                .add(Keys.DISPLAY_NAME, compactTerrainName)
                .build().createSnapshot();
        dirtItem = ItemStack.builder()
                .itemType(ItemTypes.DIRT)
                .add(Keys.DIRT_TYPE, DirtTypes.COARSE_DIRT)
                .build().createSnapshot();
        terrainCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, terrainCrystalName)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return terrainCrystalItem;
    }

    @Override
    public String getId() {
        return "terrain_crystal";
    }

    @Override
    public String getName() {
        return "terrain crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    if (!ingredient.getType().equals(compactItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(compactTerrainName)) return false;
                }
                else
                {
                    // outside ingredient
                    if (!ingredient.getType().equals(dirtItem.getType())) return false;
                    Optional<DirtType> dirtType = ingredient.get(Keys.DIRT_TYPE);
                    if (!dirtType.isPresent() || !dirtType.get().equals(DirtTypes.COARSE_DIRT)) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        // get middle slot lore item
        Optional<Slot> slot = grid.getSlot(1, 1);
        if (!slot.isPresent()) return terrainCrystalItem;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return terrainCrystalItem;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(terrainCrystalItem.getType())
                .add(Keys.DISPLAY_NAME, terrainCrystalName);

        // copy lore if it exists
        Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
        if (lore.isPresent())
        {
            // second line has event name
            if (lore.get().size() >= 2)
            {
                List<Text> customLore = Arrays.asList(lore.get().get(1));
                customItem.add(Keys.ITEM_LORE, customLore);
            }
        }

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
