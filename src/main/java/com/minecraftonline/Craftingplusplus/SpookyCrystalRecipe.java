package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class SpookyCrystalRecipe extends SpookyInfo implements CraftingRecipe {
    
    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return spookyCrystalItem;
    }

    @Override
    public String getId() {
        return "spooky_crystal";
    }

    @Override
    public String getName() {
        return "spooky crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    // only allow the pumpkin / lit pumpkin variants, not the furnace kind
                    // as that is a reward from the boss summoned by this crystal
                    if (!(ingredient.getType().equals(ItemTypes.PUMPKIN) ||
                          ingredient.getType().equals(ItemTypes.LIT_PUMPKIN))) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(pumpkinName)) return false;
                }
                else if (y == 2 && x == 1)
                {
                    // bottom middle ingredient
                    if (!ingredient.getType().equals(ItemTypes.NETHER_STAR)) return false;
                }
                else {
                    // outside ingredient
                    if (!ingredient.getType().equals(ItemTypes.VINE)) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return spookyCrystalItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
