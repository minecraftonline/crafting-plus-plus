package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class VinesLeggingsRecipe extends SpookyInfo implements CraftingRecipe {
    private ItemStackSnapshot leggingsItem;
    private Text leggingsName = Text.of(TextColors.GREEN, "Wrappings of Vines");
    public static List<Text> leggingsLore = Arrays.asList(Text.of(TextColors.GRAY, "Woven together from vines"));
    {
        leggingsItem = ItemStack.builder()
                .itemType(ItemTypes.CHAINMAIL_LEGGINGS)
                .add(Keys.DISPLAY_NAME, leggingsName)
                .add(Keys.ITEM_LORE, vineLore)
                .add(Keys.ITEM_ENCHANTMENTS, vineEnchants)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return leggingsItem;
    }

    @Override
    public String getId() {
        return "vines_leggings";
    }

    @Override
    public String getName() {
        return "vines leggings";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (!(x == 1 && (y == 1 || y == 2)))
                {
                    // everything besides middle and bottom middle
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    if (y == 0 && x != 1)
                    {
                        // top corners
                        if (!ingredient.getType().equals(ItemTypes.PUMPKIN_PIE)) return false;
                        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                        if (!name.isPresent() || !name.get().equals(pumpkinPieName)) return false;
                    }
                    else
                    {
                        if (!ingredient.getType().equals(ItemTypes.VINE)) return false;
                    }
                }
                else
                {
                    // empty spot (middle bottom)
                    if (slotItem.isPresent()) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return leggingsItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
