package com.minecraftonline.Craftingplusplus;

public class DaylightNuggetsRecipe extends NuggetsRecipe {

    @Override
    public String getId() {
        return "daylight_nuggets_from_ingot";
    }

    @Override
    public String getName() {
        return "daylight nuggets from ingot";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return DaylightInfo.getInstance();
    }

}
