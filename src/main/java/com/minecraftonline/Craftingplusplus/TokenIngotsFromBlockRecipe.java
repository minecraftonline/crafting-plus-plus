package com.minecraftonline.Craftingplusplus;

public class TokenIngotsFromBlockRecipe extends IngotsFromBlockRecipe {

    @Override
    public String getId() {
        return "token_ingots_from_block";
    }

    @Override
    public String getName() {
        return "token ingots from block";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return TokenInfo.getInstance();
    }

}
