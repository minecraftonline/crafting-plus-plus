package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class DaylightInfo extends BlockIngotNuggetInfo {

    private static DaylightInfo instance = null;

    public DaylightInfo() {
        super();
        nuggetName = Text.of(TextColors.YELLOW, "Daylight Nugget");
        ingotName = Text.of(TextColors.YELLOW, "Daylight Ingot");
        blockName = Text.of(TextColors.YELLOW, "Daylight Block");
        nuggetLore = Arrays.asList(Text.of(TextColors.GRAY, "A shining nugget of daylight"));
        ingotLore = Arrays.asList(Text.of(TextColors.GRAY, "A shining ingot of daylight"));
        blockLore = Arrays.asList(Text.of(TextColors.GRAY, "A shining block of daylight"));
        initialise();
    }

    public static DaylightInfo getInstance() {
        if (instance == null) instance = new DaylightInfo();
        return instance;
    }

}
