package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class TokenInfo extends BlockIngotNuggetInfo {

    private static TokenInfo instance = null;

    public TokenInfo() {
        super();
        nuggetName = Text.of(TextColors.GOLD, "\"tOkEn\"");
        ingotName = Text.of(TextColors.GOLD, "\"ToKeN BaR\"");
        blockName = Text.of(TextColors.GOLD, "\"TOKEN CUBE\"");
        nuggetLore = Arrays.asList(Text.of("Not redeemable in the store."), Text.of("No cash value."));
        ingotLore = Arrays.asList(Text.of("Not redeemable in the store."), Text.of(TextColors.GREEN, "[\u03B1]"));
        blockLore = Arrays.asList(Text.of("Not redeemable in the store."), Text.of(TextColors.DARK_BLUE, "[\u03B2]"));
        initialise();
    }

    public static TokenInfo getInstance() {
        if (instance == null) instance = new TokenInfo();
        return instance;
    }

}
