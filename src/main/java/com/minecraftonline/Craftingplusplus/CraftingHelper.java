package com.minecraftonline.Craftingplusplus;

import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class CraftingHelper {

    public static Text uncommonLore = Text.of(TextColors.GREEN, "[\u03B1]");
    public static Text alternateLore = Text.of(TextColors.DARK_AQUA, "[\u03A3]");

    public static ItemStack.Builder copyExtras(ItemStack ingredient, ItemStack.Builder builder)
    {
        Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
        if (lore.isPresent()) builder.add(Keys.ITEM_LORE, lore.get());

        Optional<List<Enchantment>> enchants = ingredient.get(Keys.ITEM_ENCHANTMENTS);
        if (enchants.isPresent()) builder.add(Keys.ITEM_ENCHANTMENTS, enchants.get());

        Optional<Boolean> hideEnchants = ingredient.get(Keys.HIDE_ENCHANTMENTS);
        if (hideEnchants.isPresent()) builder.add(Keys.HIDE_ENCHANTMENTS, hideEnchants.get());

        return builder;
    }

    public static boolean validItemInSlot(Optional<Slot> slot) {
        if (!slot.isPresent()) return false;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return false;

        if (slotItem.get().isEmpty()) return false;

        return true;
    }

    public static boolean allItemsHaveSameLore(CraftingGridInventory grid)
    {
        Optional<List<Text>> itemLore = null;
        for (int x = 0; x < grid.getRows(); x++)
        {
            for (int y = 0; y < grid.getColumns(); y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) continue;

                ItemStack ingredient = slot.get().peek().get();

                Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
                if (itemLore == null)
                {
                    itemLore = lore;
                }
                else
                {
                    // make sure lore is the same on all the items
                    if (!lore.equals(itemLore)) return false;
                }
            }
        }
        return true;
    }

    public static Optional<ItemStack> getFirstItem(CraftingGridInventory grid)
    {
        for (int x = 0; x < grid.getRows(); x++)
        {
            for (int y = 0; y < grid.getColumns(); y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) continue;

                return slot.get().peek();
            }
        }
        return Optional.empty();
    }

    public static Optional<ItemStack> getOnlyItem(CraftingGridInventory grid) {
        // get the only item in this grid, if there is only one item

        Optional<ItemStack> item = Optional.empty();
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) continue;

                if (item.isPresent()) return Optional.empty(); // already had an item
                item = slot.get().peek(); // first item that is present
            }
        }

        return item;
    }

    public static Optional<ItemStack> getOnlyNItems(CraftingGridInventory grid, int amount) {
        // get amount of the same items in this grid, if they are the only items

        Optional<ItemStack> item = Optional.empty();
        int count = 0;
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) continue;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (item.isPresent()) {
                    count++;
                    if (count > amount) return Optional.empty(); // too many items
                    if (!itemsAreSameIgnoreQuantity(item.get(), slotItem.get())) return Optional.empty(); // items not the same
                } else {
                    count++;
                    item = slotItem; // first item
                }
            }
        }

        if (count < amount) return Optional.empty();

        return item;
    }

    public static Optional<ItemStack> getAllSameItem(CraftingGridInventory grid) {
        return getAllSameItem(grid, 3); // default to 3x3 grid
    }

    public static boolean itemsAreSameIgnoreQuantity(ItemStack first, ItemStack second) {
        // send copies so the original quantities are not changed
        return itemsAreSameSetQuantity(first.copy(), second.copy());
    }

    private static boolean itemsAreSameSetQuantity(ItemStack first, ItemStack second) {
        first.setQuantity(1);
        second.setQuantity(1);
        return first.equalTo(second);
    }

    public static Optional<ItemStack> getAllSameItem(CraftingGridInventory grid, int size) {
        // all slots must be the same item

        Optional<ItemStack> item = Optional.empty();

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                // return empty if an item is missing
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) return Optional.empty();

                Optional<ItemStack> slotItem = slot.get().peek();

                if (item.isPresent()) {
                    if (!itemsAreSameIgnoreQuantity(item.get(), slotItem.get())) return Optional.empty(); // items not the same
                } else item = slotItem; // first item
            }
        }

        return item;
    }

    public static int getItemCount(CraftingGridInventory grid)
    {
        int count = 0;
        for (int x = 0; x < grid.getRows(); x++)
        {
            for (int y = 0; y < grid.getColumns(); y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!validItemInSlot(slot)) continue;

                count++;
            }
        }
        return count;
    }

    public static boolean slotIsEmpty(Optional<Slot> slot)
    {
        return !validItemInSlot(slot);
    }

}
