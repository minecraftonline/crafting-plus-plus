package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ChocolateExtension extends ChocolateInfo {

    private static ChocolateExtension instance = null;

    public ChocolateExtension() {
        super();
        initialise();
    }

    public static ChocolateExtension getInstance() {
        if (instance == null) instance = new ChocolateExtension();
        return instance;
    }

}
