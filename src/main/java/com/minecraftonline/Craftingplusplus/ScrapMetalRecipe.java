package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class ScrapMetalRecipe extends MetalInfo implements CraftingRecipe {

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return metalItem;
    }

    @Override
    public String getId() {
        return "scrap_metal";
    }

    @Override
    public String getName() {
        return "scrap metal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        Optional<ItemStack> slotItem = CraftingHelper.getAllSameItem(grid);
        if (!slotItem.isPresent()) return false; // not enough identical items

        ItemStack ingredient = slotItem.get();

        if (!ingredient.getType().equals(metalNuggetItem.getType())) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(metalNuggetName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        Optional<ItemStack> slotItem = CraftingHelper.getFirstItem(grid);
        if (!slotItem.isPresent()) return metalItem;

        if (!CraftingHelper.allItemsHaveSameLore(grid)) return metalItem;

        ItemStack ingredient = slotItem.get();

        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(metalItem.getType())
                .add(Keys.DISPLAY_NAME, metalName);

        Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
        if (lore.isPresent() && lore.get().equals(metalNuggetLore)) {
            // add custom lore if previous also had (different) custom lore
            customItem.add(Keys.ITEM_LORE, metalLore);
        }

        ItemStack resultItem = customItem.build();

        return resultItem.createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
