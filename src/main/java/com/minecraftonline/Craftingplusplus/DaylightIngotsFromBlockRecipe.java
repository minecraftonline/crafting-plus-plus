package com.minecraftonline.Craftingplusplus;

public class DaylightIngotsFromBlockRecipe extends IngotsFromBlockRecipe {

    @Override
    public String getId() {
        return "daylight_ingots_from_block";
    }

    @Override
    public String getName() {
        return "daylight ingots from block";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return DaylightInfo.getInstance();
    }

}
