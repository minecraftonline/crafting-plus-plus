package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class HoneyCrystalRecipe extends HoneyInfo implements CraftingRecipe {

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return crystalItem;
    }

    @Override
    public String getId() {
        return "honey_crystal";
    }

    @Override
    public String getName() {
        return "honey crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    if (!ingredient.getType().equals(ItemTypes.GOLDEN_HELMET)) return false;
                }
                else if (y == 2 && x == 1)
                {
                    // bottom middle ingredient
                    if (!ingredient.getType().equals(ItemTypes.GHAST_TEAR)) return false;
                }
                else {
                    // outside ingredient
                    if (!ingredient.getType().equals(honeyItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent()) return false;

                    // either special honey item works
                    if (!name.get().equals(honeyName) && !name.get().equals(nbHoneyName)) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return crystalItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
