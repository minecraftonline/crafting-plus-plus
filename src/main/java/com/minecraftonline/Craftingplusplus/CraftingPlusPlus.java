package com.minecraftonline.Craftingplusplus;

import java.util.Optional;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.*;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameRegistryEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.item.inventory.CraftItemEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipes;
import org.spongepowered.api.item.recipe.crafting.Ingredient;
import org.spongepowered.api.item.recipe.crafting.ShapedCraftingRecipe;
import org.spongepowered.api.item.recipe.crafting.ShapelessCraftingRecipe;
import org.spongepowered.api.item.recipe.smelting.SmeltingRecipe;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.google.inject.Inject;

@Plugin(id = "craftingplusplus")
public class CraftingPlusPlus {

    private static CraftingPlusPlus INSTANCE;

    private final Logger logger;
    private final PluginContainer container;

    private final CraftingRecipe snowflakeBlockRecipe;
    private final CraftingRecipe scrapMetalRecipe;
    private final CraftingRecipe shrapnelRecipe;
    private final CraftingRecipe daylightNuggetsRecipe;
    private final CraftingRecipe daylightIngotFromNuggetsRecipe;
    private final CraftingRecipe daylightIngotsFromBlockRecipe;
    private final CraftingRecipe daylightBlockRecipe;
    private final CraftingRecipe tokenNuggetsRecipe;
    private final CraftingRecipe tokenIngotFromNuggetsRecipe;
    private final CraftingRecipe tokenIngotsFromBlockRecipe;
    private final CraftingRecipe tokenBlockRecipe;

    @Inject
    public CraftingPlusPlus(final Logger logger, final PluginContainer container) {
        this.container = container;
        INSTANCE = this;
        this.logger = logger;
        this.snowflakeBlockRecipe = new SnowflakeBlockRecipe();
        this.scrapMetalRecipe = new ScrapMetalRecipe();
        this.shrapnelRecipe = new ShrapnelRecipe();
        this.daylightNuggetsRecipe = new DaylightNuggetsRecipe();
        this.daylightIngotFromNuggetsRecipe = new DaylightIngotFromNuggetsRecipe();
        this.daylightIngotsFromBlockRecipe = new DaylightIngotsFromBlockRecipe();
        this.daylightBlockRecipe = new DaylightBlockRecipe();
        this.tokenNuggetsRecipe = new TokenNuggetsRecipe();
        this.tokenIngotFromNuggetsRecipe = new TokenIngotFromNuggetsRecipe();
        this.tokenIngotsFromBlockRecipe = new TokenIngotsFromBlockRecipe();
        this.tokenBlockRecipe = new TokenBlockRecipe();
    }

    public static CraftingPlusPlus getInstance() {
        return INSTANCE;
    }

    public static Logger getLogger() {
        return getInstance().logger;
    }

    public static String getId() {
        return getInstance().container.getId();
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("CraftingPlusPlus is starting.");
    }

    @Listener
    public void onCraftItemEvent(CraftItemEvent.Preview craftEvent) {
        if (!craftEvent.getRecipe().isPresent()) return;

        Optional<Player> player = craftEvent.getCause().first(Player.class);
        if (!player.isPresent()) return;

        World world = player.get().getWorld();

        CraftingRecipe recipe = craftEvent.getRecipe().get();
        CraftingGridInventory grid = craftEvent.getCraftingInventory().getCraftingGrid();

        // recipes that match existing ones have to be overridden here instead of just registered
        if (recipe == CraftingRecipes.SNOW) {
            if (this.snowflakeBlockRecipe.isValid(grid, world)) {
                // give snowflake blocks instead of normal snow blocks
                if (CraftingHelper.allItemsHaveSameLore(grid)) {
                    // only valid if not mixed item lores
                    craftEvent.getPreview().setCustom(this.snowflakeBlockRecipe.getResult(grid));
                } else {
                    craftEvent.getPreview().setValid(false);
                }
            }
        } else if (recipe == CraftingRecipes.IRON_INGOT_FROM_NUGGETS) {
            if (this.scrapMetalRecipe.isValid(grid, world)) {
                // give scrap metal instead of normal iron ingot
                if (CraftingHelper.allItemsHaveSameLore(grid)) {
                    // only valid if not mixed item lores
                    craftEvent.getPreview().setCustom(this.scrapMetalRecipe.getResult(grid));
                } else {
                    craftEvent.getPreview().setValid(false);
                }
            }
        } else if (recipe == CraftingRecipes.IRON_NUGGET) {
            if (this.shrapnelRecipe.isValid(grid, world)) {
                // give shrapnel instead of normal nugget
                craftEvent.getPreview().setCustom(this.shrapnelRecipe.getResult(grid));
            } // daylightNuggetsFromIngotRecipe
        } else if (recipe == CraftingRecipes.GOLD_NUGGET) {
            if (this.daylightNuggetsRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.daylightNuggetsRecipe.getResult(grid));
            } else if (this.tokenNuggetsRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.tokenNuggetsRecipe.getResult(grid));
            }
        } else if (recipe == CraftingRecipes.GOLD_INGOT_FROM_NUGGETS) {
            if (this.daylightIngotFromNuggetsRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.daylightIngotFromNuggetsRecipe.getResult(grid));
            } else if (this.tokenIngotFromNuggetsRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.tokenIngotFromNuggetsRecipe.getResult(grid));
            }
        } else if (recipe == CraftingRecipes.GOLD_INGOT_FROM_BLOCK) {
            if (this.daylightIngotsFromBlockRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.daylightIngotsFromBlockRecipe.getResult(grid));
            } else if (this.tokenIngotsFromBlockRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.tokenIngotsFromBlockRecipe.getResult(grid));
            }
        } else if (recipe == CraftingRecipes.GOLD_BLOCK) {
            if (this.daylightBlockRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.daylightBlockRecipe.getResult(grid));
            } else if (this.tokenBlockRecipe.isValid(grid, world)) {
                craftEvent.getPreview().setCustom(this.tokenBlockRecipe.getResult(grid));
            }
        }
    }

    @Listener
    public void onCustomCraftingRegistry(GameRegistryEvent.Register<CraftingRecipe> e) {
        ItemStack fourSand = ItemStack.builder().itemType(ItemTypes.SAND)
                .quantity(4)
                .build();
        Ingredient sandstone = Ingredient.of(ItemTypes.SANDSTONE);
        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(sandstone)
                .result(fourSand)
                .id("custom_sandstone")
                .name("custom sandstone")
                .group(getId())
                .build());

        ItemStack podzol = ItemStack.builder().itemType(ItemTypes.DIRT)
                .add(Keys.DIRT_TYPE, DirtTypes.PODZOL)
                .quantity(1)
                .build();
        Ingredient bonemeal = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, DyeColors.WHITE)
                .build());
        Ingredient leaves = Ingredient.of(ItemTypes.LEAVES);
        Ingredient dirt = Ingredient.of(ItemTypes.DIRT);
        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(leaves)
                .addIngredient(dirt)
                .addIngredient(bonemeal)
                .result(podzol)
                .id("custom_podzol")
                .name("custom podzol")
                .group(getId())
                .build());

        ItemStack fourRedSand = ItemStack.builder().itemType(ItemTypes.SAND)
                .add(Keys.SAND_TYPE, SandTypes.RED)
                .quantity(4)
                .build();
        Ingredient redSandstone = Ingredient.of(ItemTypes.RED_SANDSTONE);
        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(redSandstone)
                .result(fourRedSand)
                .id("custom_red_sandstone")
                .name("custom red sandstone")
                .group(getId())
                .build());

        ItemStack eightRedSand = ItemStack.builder().itemType(ItemTypes.SAND)
                .add(Keys.SAND_TYPE, SandTypes.RED)
                .quantity(8)
                .build();
        Ingredient sand = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.SAND)
                .add(Keys.SAND_TYPE, SandTypes.NORMAL)
                .build());
        Ingredient redDye = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, DyeColors.RED)
                .build());
        e.register(ShapedCraftingRecipe.builder()
                .aisle("WWW", "WSW", "WWW")
                .where('W', sand)
                .where('S', redDye)
                .result(eightRedSand)
                .id("custom_red_sand")
                .name("custom red sand")
                .group(getId())
                .build());

        Ingredient stick = Ingredient.of(ItemTypes.STICK);
        e.register(ShapedCraftingRecipe.builder()
                .aisle(" S ", "SSS", " S ")
                .where('S', stick)
                .result(ItemStack.of(ItemTypes.DEADBUSH))
                .id("custom_deadbush")
                .name("custom dead bush")
                .group(getId())
                .build());

        Ingredient yellowWool = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.WOOL)
                .add(Keys.DYE_COLOR, DyeColors.YELLOW)
                .build());
        Ingredient slime = Ingredient.of(ItemTypes.SLIME);
        e.register(ShapedCraftingRecipe.builder()
                .aisle("WSW", "SWS", "WSW")
                .where('W', yellowWool)
                .where('S', slime)
                .result(ItemStack.of(ItemTypes.SPONGE))
                .id("custom_sponge")
                .name("custom sponge")
                .group(getId())
                .build());

        //Recipe for Nether Brick Fence from 1.14 18w43a onwards. REMOVE AFTER SERVER VERSION UPDATE
        ItemStack sixNetherBrickFence = ItemStack.builder().itemType(ItemTypes.NETHER_BRICK_FENCE)
                .quantity(6)
                .build();
        Ingredient netherBricks = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.NETHER_BRICK)
                .build());
        Ingredient netherBrick = Ingredient.of(ItemTypes.NETHERBRICK);
        e.register(ShapedCraftingRecipe.builder()
                .aisle("BNB", "BNB")
                .where('B', netherBricks)
                .where('N', netherBrick)
                .result(sixNetherBrickFence)
                .id("custom_nether_brick_fence")
                .name("custom nether brick fence")
                .group(getId())
                .build());

        ItemType[] dyeableTypes = new ItemType[]{ItemTypes.STAINED_GLASS, ItemTypes.STAINED_GLASS_PANE, ItemTypes.STAINED_HARDENED_CLAY};
        for (DyeColor dyeColor : Sponge.getRegistry().getAllOf(DyeColor.class)) {
            ItemStack powder = ItemStack.builder().itemType(ItemTypes.CONCRETE_POWDER)
                    .add(Keys.DYE_COLOR, dyeColor).build();

            ItemStack concrete = ItemStack.builder().itemType(ItemTypes.CONCRETE)
                    .add(Keys.DYE_COLOR, dyeColor).quantity(8).build();

            e.register(ShapedCraftingRecipe.builder()
                    .aisle("PPP", "PBP", "PPP")
                    .where('B', Ingredient.of(ItemTypes.WATER_BUCKET))
                    .where('P', Ingredient.of(powder))
                    .result(concrete)
                    .id("concrete_" + dyeColor.getId())
                    .name(dyeColor.getName() + " concrete")
                    .group(getId())
                    .build());

            for (ItemType dyeableType : dyeableTypes) {

                ItemStack dyedItem = ItemStack.builder().itemType(dyeableType)
                        .add(Keys.DYE_COLOR, dyeColor).quantity(8).build();
                Ingredient dyeableItem = Ingredient.of(dyeableType);
                Ingredient dye = Ingredient.of(ItemStack.builder().itemType(ItemTypes.DYE).add(Keys.DYE_COLOR, dyeColor).build());

                e.register(ShapedCraftingRecipe.builder()
                        .aisle("GGG", "GDG", "GGG")
                        .where('G', dyeableItem)
                        .where('D', dye)
                        .result(dyedItem)
                        .id("dye_" + dyeableType.getId() + "_" + dyeColor.getId())
                        .name("Dye " + dyeableType.getName() + " " + dyeColor.getName())
                        .group(getId())
                        .build());
            }
        }

        ItemStack glutenFreeBread = ItemStack.builder().itemType(ItemTypes.BREAD)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Gluten Free Bread"))
                .build();
        Ingredient wheat = Ingredient.of(ItemTypes.WHEAT);
        e.register(ShapedCraftingRecipe.builder()
                .aisle("B", "B", "B")
                .where('B', wheat)
                .result(glutenFreeBread)
                .id("gluten_free_bread")
                .name("gluten free bread")
                .group(getId())
                .build());

        ItemStack twoDiorite = ItemStack.builder().itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.DIORITE)
                .quantity(2)
                .build();
        Ingredient cobble = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.COBBLESTONE)
                .build());
        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(cobble)
                .addIngredient(bonemeal)
                .addIngredient(cobble)
                .addIngredient(bonemeal)
                .result(twoDiorite)
                .id("custom_diorite")
                .name("custom diorite")
                .group(getId())
                .build());

        Ingredient diorite = Ingredient.of(
                ItemStack.builder().itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.DIORITE)
                .build());
        ItemStack granite = ItemStack.builder().itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.GRANITE)
                .build();

        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(diorite)
                .addIngredient(bonemeal)
                .result(granite)
                .id("custom_granite")
                .name("custom granite")
                .group(getId())
                .build());

        ItemStack packedIce = ItemStack.builder().itemType(ItemTypes.PACKED_ICE)
                .build();
        Ingredient ice = Ingredient.of(ItemTypes.ICE);
        e.register(ShapedCraftingRecipe.builder()
                .aisle("BBB", "BBB", "BBB")
                .where('B', ice)
                .result(packedIce)
                .id("packed_ice")
                .name("packedice")
                .group(getId())
                .build());

        ItemStack blazeRod = ItemStack.builder()
                .itemType(ItemTypes.BLAZE_ROD)
                .quantity(1)
                .build();
        e.register(ShapelessCraftingRecipe.builder()
                .addIngredient(Ingredient.of(ItemTypes.STICK))
                .addIngredient(Ingredient.of(ItemTypes.BLAZE_POWDER))
                .addIngredient(Ingredient.of(ItemTypes.BLAZE_POWDER))
                .result(blazeRod)
                .id("custom_blaze_rod")
                .name("custom blaze rod")
                .group(getId())
                .build());

        e.register(new SnowflakeCrystalRecipe());
        e.register(new SnowflakeRecipe());
        e.register(new MetalCrystalRecipe());
        e.register(new TerrainCrystalRecipe());
        e.register(new CompactTerrainRecipe());
        e.register(new ChocolateCrystalRecipe());
        e.register(new CompletedPromiseRecipe());
        e.register(new BeeArmourRecipe());
        e.register(new HoneyCrystalRecipe());
        e.register(new SpookyCrystalRecipe());
        e.register(new VinesHelmetRecipe());
        e.register(new VinesChestplateRecipe());
        e.register(new VinesLeggingsRecipe());
        e.register(new VinesBootsRecipe());
        e.register(new AstralCrystalRecipe());
        e.register(new ChocolateBlockRecipe());
        e.register(new ChocolateNuggetsRecipe());
        e.register(new ChocolateIngotsFromBlockRecipe());
        e.register(new ChocolateIngotFromNuggetsRecipe());
        e.register(new TemporalCrystalRecipe());
        e.register(new PetrifiedSoilRecipe());
    }

    @Listener
    public void onSmeltingRegistry(GameRegistryEvent.Register<SmeltingRecipe> e) {
        e.register(SmeltingRecipe.builder()
                .ingredient(ItemTypes.SNOW)
                .result(ItemStack.of(ItemTypes.ICE))
                .id("snow_to_ice")
                .name("snow to ice")
                .build());
    }

}



