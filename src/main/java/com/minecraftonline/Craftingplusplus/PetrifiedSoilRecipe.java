package com.minecraftonline.Craftingplusplus;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class PetrifiedSoilRecipe implements CraftingRecipe {

    public static ItemStackSnapshot petrifiedItem = ItemStack.builder()
            .itemType(ItemTypes.DIRT)
            .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD,"Petrified Soil"))
            .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Tainted by the remnants of a malevolent soul")))
            .build().createSnapshot();

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return petrifiedItem;
    }

    @Override
    public String getId() {
        return "petrified soil";
    }

    @Override
    public String getName() {
        return "petrified soil";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (x == 0 || x == 2)
                {
                    // Left and right columns
                    if (y == 1)
                    {
                        if (!slotItem.isPresent()) return false;
                        ItemStack ingredient = slotItem.get();
                        if (!ingredient.getType().equals(ItemTypes.DIRT)) return false;
                    }
                    else if (slotItem.isPresent()) return false;
                }

                if (x == 1)
                {
                    //middle column
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    if (y == 0 || y == 2)
                    {
                        if (!ingredient.getType().equals(ItemTypes.DIRT)) return false;
                    }
                    else
                    {
                        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                        if (!ingredient.getType().equals(ItemTypes.BONE) || !name.isPresent()
                                || !name.get().equals(Text.of(TextColors.WHITE,"Empresses Rib"))) return false;
                    }
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return petrifiedItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}



