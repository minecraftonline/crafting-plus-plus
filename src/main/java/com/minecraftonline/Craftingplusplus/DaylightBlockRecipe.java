package com.minecraftonline.Craftingplusplus;

public class DaylightBlockRecipe extends BlockRecipe {

    @Override
    public String getId() {
        return "daylight_block";
    }

    @Override
    public String getName() {
        return "daylight block";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return DaylightInfo.getInstance();
    }

}
