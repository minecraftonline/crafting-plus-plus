package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class SnowflakeRecipe extends SnowflakeInfo implements CraftingRecipe {
    
    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return snowflakeItemFromBlock;
    }

    @Override
    public String getId() {
        return "snowflake";
    }

    @Override
    public String getName() {
        return "snowflake";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {

        Optional<ItemStack> slotItem = CraftingHelper.getOnlyItem(grid);
        if (!slotItem.isPresent()) return false; // no or too many items

        ItemStack ingredient = slotItem.get();

        if (!ingredient.getType().equals(snowflakeBlockItem.getType())) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(snowflakeBlockName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        Optional<ItemStack> slotItem = CraftingHelper.getOnlyItem(grid);
        if (!slotItem.isPresent()) return snowflakeItemFromBlock;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(snowflakeItemFromBlock.getType())
                .add(Keys.DISPLAY_NAME, snowflakeName);

        customItem = CraftingHelper.copyExtras(ingredient, customItem);

        ItemStack resultItem = customItem.build();
        resultItem.setQuantity(snowflakeItemFromBlock.getQuantity());

        return resultItem.createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
