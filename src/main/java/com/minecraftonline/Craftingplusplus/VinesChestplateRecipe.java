package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.Optional;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class VinesChestplateRecipe extends SpookyInfo implements CraftingRecipe {
    private ItemStackSnapshot chestplateItem;
    private Text chestplateName = Text.of(TextColors.GREEN, "Covering of Vines");
    {
        chestplateItem = ItemStack.builder()
                .itemType(ItemTypes.CHAINMAIL_CHESTPLATE)
                .add(Keys.DISPLAY_NAME, chestplateName)
                .add(Keys.ITEM_LORE, vineLore)
                .add(Keys.ITEM_ENCHANTMENTS, vineEnchants)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return chestplateItem;
    }

    @Override
    public String getId() {
        return "vines_chestplate";
    }

    @Override
    public String getName() {
        return "vines chestplate";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    // only allow the furnace variant
                    // as that is a reward from the boss summoned by this crystal
                    if (!ingredient.getType().equals(ItemTypes.FURNACE)) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(pumpkinName)) return false;
                }
                else if (!(y == 0 && x == 1))
                {
                    // all but the top middle
                    if (!slotItem.isPresent()) return false;
                    ItemStack ingredient = slotItem.get();
                    if (y == 0)
                    {
                        // top 2 corners
                        if (!ingredient.getType().equals(ItemTypes.PUMPKIN_PIE)) return false;
                        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                        if (!name.isPresent() || !name.get().equals(pumpkinPieName)) return false;
                    }
                    else
                    {
                        if (!ingredient.getType().equals(ItemTypes.VINE)) return false;
                    }
                }
                else
                {
                    // empty spot (middle top)
                    if (slotItem.isPresent()) return false;
                }
            }
        }

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return chestplateItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
