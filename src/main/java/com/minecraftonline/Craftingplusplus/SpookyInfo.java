package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.google.common.collect.Lists;

public class SpookyInfo {

    public static ItemStackSnapshot pumpkinItem;
    public static ItemStackSnapshot spookyCrystalItem;
    public static Text pumpkinName = Text.of(TextColors.GOLD, "Spooky Pumpkin");
    public static Text pumpkinPieName = Text.of(TextColors.GREEN, "Slice of Pumpking pie");
    public static Text spookyCrystalName = Text.of(TextColors.GOLD, "Spooky Crystal");
    public static List<Text> vineLore = Arrays.asList(Text.of(TextColors.GRAY, "Woven together from haunted vines"));
    public static List<Enchantment> vineEnchants = Lists.newArrayList(
            Enchantment.of(EnchantmentTypes.MENDING, 1), Enchantment.of(EnchantmentTypes.UNBREAKING, 2));

    public SpookyInfo()
    {
        checkInitialized();
    }

    private static boolean initialized = false;

    public static void initialize() {
        // these have to be created later so that the builders are defined
        pumpkinItem = ItemStack.builder()
                .itemType(ItemTypes.PUMPKIN)
                .add(Keys.DISPLAY_NAME, pumpkinName)
                .build().createSnapshot();
        spookyCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, spookyCrystalName)
                .build().createSnapshot();

        initialized = true;
    }

    public static void checkInitialized() {
        if (!initialized)
        {
            initialize();
        }
    }

}
