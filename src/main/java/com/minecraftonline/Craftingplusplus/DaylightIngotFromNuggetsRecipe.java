package com.minecraftonline.Craftingplusplus;

public class DaylightIngotFromNuggetsRecipe extends IngotFromNuggetsRecipe {

    @Override
    public String getId() {
        return "daylight_ingot_from_nuggets";
    }

    @Override
    public String getName() {
        return "daylight ingot from nuggets";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return DaylightInfo.getInstance();
    }

}
