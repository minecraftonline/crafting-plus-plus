package com.minecraftonline.Craftingplusplus;

public class TokenIngotFromNuggetsRecipe extends IngotFromNuggetsRecipe {

    @Override
    public String getId() {
        return "token_ingot_from_nuggets";
    }

    @Override
    public String getName() {
        return "token ingot from nuggets";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return TokenInfo.getInstance();
    }

}
