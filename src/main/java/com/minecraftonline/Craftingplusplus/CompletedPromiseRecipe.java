package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class CompletedPromiseRecipe implements CraftingRecipe {

    final Text forgottenName = Text.of(TextColors.GREEN, "Forgotten Words");
    final Text completedName = Text.of(TextColors.DARK_AQUA, "Completed Promise");
    final ItemType forgottenType = ItemTypes.SPIDER_EYE;
    final ItemType completedType = ItemTypes.APPLE;
    final ItemStackSnapshot completedItem;

    CompletedPromiseRecipe()
    {
        completedItem = ItemStack.builder()
                .itemType(completedType)
                .add(Keys.DISPLAY_NAME, completedName)
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "When two halves become whole"), CraftingHelper.alternateLore))
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return completedItem;
    }

    @Override
    public String getId() {
        return "completed_promise";
    }

    @Override
    public String getName() {
        return "completed promise";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        Optional<ItemStack> slotItem = CraftingHelper.getOnlyNItems(grid, 2);

        if (!slotItem.isPresent()) return false;

        ItemStack ingredient = slotItem.get();

        if (!ingredient.getType().equals(forgottenType)) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(forgottenName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return completedItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
