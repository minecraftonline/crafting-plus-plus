package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

public class HoneyInfo {

    public static ItemStackSnapshot honeyItem;
    public static ItemStackSnapshot armourItem;
    public static ItemStackSnapshot crystalItem;
    public static Text honeyName = Text.of(TextColors.RESET, "Manuka Honey");
    public static Text nbHoneyName = Text.of(TextColors.RESET, "Nonbinary Honey");
    public static Text armourName = Text.of(TextColors.YELLOW, "Bee Helmet");
    public static List<Text> armourLore = Arrays.asList(Text.of(TextColors.GRAY, "Soaked in honey"));
    public static List<Text> nbArmourLore = Arrays.asList(Text.of(TextColors.GRAY, "Soaked in nonbinary honey"));

    public HoneyInfo()
    {
        checkInitialized();
    }

    private static boolean initialized = false;

    public static void initialize()
    {
        honeyItem = ItemStack.builder()
                .itemType(ItemTypes.POTION)
                .add(Keys.POTION_COLOR, Color.ofRgb(16101432))
                .add(Keys.DISPLAY_NAME, honeyName)
                .build().createSnapshot();
        armourItem = ItemStack.builder()
                .itemType(ItemTypes.LEATHER_HELMET)
                .add(Keys.COLOR, Color.ofRgb(16763904))
                .add(Keys.DISPLAY_NAME, armourName)
                .build().createSnapshot();
        crystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Honey Crystal"))
                .build().createSnapshot();
    }

    public static void checkInitialized()
    {
        if (!initialized)
        {
            initialize();
        }
    }
}
