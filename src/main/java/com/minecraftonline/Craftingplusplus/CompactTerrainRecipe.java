package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.StoneTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class CompactTerrainRecipe extends TwoByTwoRecipe implements CraftingRecipe {
    private ItemStackSnapshot compactItem;
    private ItemStackSnapshot dirtItem;
    private Text compactTerrainName = Text.of(TextColors.GOLD, "Compact Terrain");
    private Text dirtName = Text.of(TextColors.GOLD, "Petrified Soil");
    private List<Text> compactTerrainLore = Arrays.asList(Text.of(TextColors.GRAY, "Heavier than expected"));
    {
        compactItem = ItemStack.builder()
                .itemType(ItemTypes.STONE)
                .add(Keys.STONE_TYPE, StoneTypes.SMOOTH_GRANITE)
                .add(Keys.DISPLAY_NAME, compactTerrainName)
                .add(Keys.ITEM_LORE, compactTerrainLore)
                .build().createSnapshot();
        dirtItem = ItemStack.builder()
                .itemType(ItemTypes.DIRT)
                .add(Keys.DISPLAY_NAME, dirtName)
                .build().createSnapshot();
    }

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return compactItem;
    }

    @Override
    public String getId() {
        return "compact_terrain";
    }

    @Override
    public String getName() {
        return "compact terrain";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        return isTwoByTwo(grid);
    }

    @Override
    protected boolean hasRequiredItem(Optional<Slot> slot)
    {
        if (!slot.isPresent()) return false;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return false;

        ItemStack item = slotItem.get();

        if (!item.getType().equals(dirtItem.getType())) return false;
        Optional<Text> name = item.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(dirtName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        Optional<ItemStack> slotItem = CraftingHelper.getFirstItem(grid);
        if (!slotItem.isPresent()) return compactItem;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(compactItem.getType())
                .add(Keys.STONE_TYPE, compactItem.get(Keys.STONE_TYPE).get())
                .add(Keys.DISPLAY_NAME, compactTerrainName)
                .add(Keys.ITEM_LORE, compactTerrainLore);

        // only change lore if all the items have matching lore
        if (CraftingHelper.allItemsHaveSameLore(grid))
        {
            Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
            if (lore.isPresent())
            {
                // second line has event name
                if (lore.get().size() >= 2)
                {
                    List<Text> customLore = Arrays.asList(compactTerrainLore.get(0), lore.get().get(1));
                    customItem.add(Keys.ITEM_LORE, customLore);
                }
            }
        }

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
