package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class SnowflakeCrystalRecipe extends SnowflakeInfo implements CraftingRecipe {
    
    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return snowflakeCrystalItem;
    }

    @Override
    public String getId() {
        return "snowflake_crystal";
    }

    @Override
    public String getName() {
        return "snowflake crystal";
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (y == 1 && x == 1)
                {
                    // middle ingredient
                    if (!ingredient.getType().equals(snowflakeBlockItem.getType())) return false;
                    Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
                    if (!name.isPresent() || !name.get().equals(snowflakeBlockName)) return false;
                }
                else
                {
                    // outside ingredient
                    if (!ingredient.getType().equals(ItemTypes.ICE)) return false;
                }
            }
        }
        
        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        // get middle slot lore item
        Optional<Slot> slot = grid.getSlot(1, 1);
        if (!slot.isPresent()) return snowflakeCrystalItem;

        Optional<ItemStack> slotItem = slot.get().peek();
        if (!slotItem.isPresent()) return snowflakeCrystalItem;

        ItemStack ingredient = slotItem.get();

        // copy data from item used in crafting
        ItemStack.Builder customItem = ItemStack.builder()
                .itemType(snowflakeCrystalItem.getType())
                .add(Keys.DISPLAY_NAME, snowflakeCrystalName);

        // copy lore if it exists
        Optional<List<Text>> lore = ingredient.get(Keys.ITEM_LORE);
        if (lore.isPresent()) customItem.add(Keys.ITEM_LORE, lore.get());

        return customItem.build().createSnapshot();
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

}
