package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.google.common.collect.Lists;

public abstract class ChocolateInfo {

    public ItemStackSnapshot chocolateNuggetItem;
    public ItemStackSnapshot chocolateNuggetItemFromIngot;
    public ItemStackSnapshot chocolateIngotItem;
    public ItemStackSnapshot chocolateIngotItemFromBlock;
    public ItemStackSnapshot chocolateBlockItem;
    public static Text nuggetName = Text.of(TextColors.GOLD, "Chocolate Candies");
    public static Text nuggetName2 = Text.of(TextColors.DARK_PURPLE, "Chocolates");
    public static Text nuggetName3 = Text.of(TextColors.GREEN, "Chocolate Candies");
    public static Text ingotName = Text.of(TextColors.GOLD, "Chocolate Bar");
    public static Text blockName = Text.of(TextColors.GOLD, "Chocolate Cube");
    public static List<Text> nuggetLore = Arrays.asList(Text.of(TextColors.LIGHT_PURPLE, "Yummy!"));
    public static List<Text> ingotLore = Arrays.asList(Text.of(TextColors.LIGHT_PURPLE, "Delicious!"));
    public static List<Text> blockLore = Arrays.asList(Text.of(TextColors.LIGHT_PURPLE, "Scrumptious!"));
    private static int quantity = 9;

    public void initialise()
    {
        chocolateNuggetItem = ItemStack.builder()
                .itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, DyeColors.BROWN)
                .add(Keys.DISPLAY_NAME, nuggetName)
                .add(Keys.ITEM_LORE, nuggetLore)
                .build().createSnapshot();
        ItemStack chocolateNuggetFromIngot = ItemStack.builder()
                .itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, DyeColors.BROWN)
                .add(Keys.DISPLAY_NAME, nuggetName)
                .add(Keys.ITEM_LORE, nuggetLore)
                .build();
        chocolateNuggetFromIngot.setQuantity(quantity);
        chocolateNuggetItemFromIngot = chocolateNuggetFromIngot.createSnapshot();
        chocolateIngotItem = ItemStack.builder()
                .itemType(ItemTypes.BRICK)
                .add(Keys.DISPLAY_NAME, ingotName)
                .add(Keys.ITEM_LORE, ingotLore)
                .build().createSnapshot();
        ItemStack chocolateIngotFromBlock = ItemStack.builder()
                .itemType(ItemTypes.BRICK)
                .add(Keys.DISPLAY_NAME, ingotName)
                .add(Keys.ITEM_LORE, ingotLore)
                .build();
        chocolateIngotFromBlock.setQuantity(quantity);
        chocolateIngotItemFromBlock = chocolateIngotFromBlock.createSnapshot();
        chocolateBlockItem = ItemStack.builder()
                .itemType(ItemTypes.BRICK_BLOCK)
                .add(Keys.DISPLAY_NAME, blockName)
                .add(Keys.ITEM_LORE, blockLore)
                .build().createSnapshot();
    }

}
