package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.minecraftonline.Craftingplusplus.ChocolateInfo.nuggetName;
import static com.minecraftonline.Craftingplusplus.ChocolateInfo.nuggetName2;
import static com.minecraftonline.Craftingplusplus.ChocolateInfo.nuggetName3;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public class ChocolateIngotFromNuggetsRecipe implements CraftingRecipe {

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return getInfo().chocolateIngotItem;
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                Optional<Slot> slot = grid.getSlot(x, y);
                if (!slot.isPresent()) return false;

                Optional<ItemStack> slotItem = slot.get().peek();
                if (!slotItem.isPresent()) return false;

                ItemStack ingredient = slotItem.get();

                if (!(isChocolateNuggetItem(ingredient))) return false;

            }

        }

        return true;
    }

    public static boolean isChocolateNuggetItem(ItemStack ingredient) {
        if (!ingredient.getType().equals(ItemTypes.DYE)) return false;
        Optional<DyeColor> dyeColor = ingredient.get(Keys.DYE_COLOR);
        if (!dyeColor.isPresent() || !dyeColor.get().equals(DyeColors.BROWN)) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent()) return false;
        return (name.get().equals(nuggetName) || name.get().equals(nuggetName2) || name.get().equals(nuggetName3));
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return getInfo().chocolateIngotItem;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

    @Override
    public String getId() {
        return "chocolate_ingot_from_nuggets";
    }

    @Override
    public String getName() {
        return "chocolate ingot from nuggets";
    }

    protected ChocolateInfo getInfo() {
        return ChocolateExtension.getInstance();
    }

}
