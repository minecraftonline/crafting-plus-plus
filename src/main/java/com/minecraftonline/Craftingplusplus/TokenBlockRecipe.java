package com.minecraftonline.Craftingplusplus;

public class TokenBlockRecipe extends BlockRecipe {

    @Override
    public String getId() {
        return "token_block";
    }

    @Override
    public String getName() {
        return "token block";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return TokenInfo.getInstance();
    }

}
