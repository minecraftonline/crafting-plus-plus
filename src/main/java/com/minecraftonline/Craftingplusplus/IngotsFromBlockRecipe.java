package com.minecraftonline.Craftingplusplus;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public abstract class IngotsFromBlockRecipe implements CraftingRecipe {

    @Override
    public ItemStackSnapshot getExemplaryResult() {
        return getInfo().ingotItemFromBlock;
    }

    @Override
    public boolean isValid(CraftingGridInventory grid, World world) {
        Optional<ItemStack> slotItem = CraftingHelper.getOnlyItem(grid);
        if (!slotItem.isPresent()) return false; // not enough or too many items

        ItemStack ingredient = slotItem.get();

        if (!ingredient.getType().equals(getInfo().blockItem.getType())) return false;
        Optional<Text> name = ingredient.get(Keys.DISPLAY_NAME);
        if (!name.isPresent() || !name.get().equals(getInfo().blockName)) return false;

        return true;
    }

    @Override
    public ItemStackSnapshot getResult(CraftingGridInventory grid) {
        return getInfo().ingotItemFromBlock;
    }

    @Override
    public List<ItemStackSnapshot> getRemainingItems(CraftingGridInventory grid) {
        // remaining items must be the same size
        return Collections.nCopies(grid.capacity(), ItemStackSnapshot.NONE);
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.of(CraftingPlusPlus.getId());
    }

    protected abstract BlockIngotNuggetInfo getInfo();

}
