package com.minecraftonline.Craftingplusplus;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class MetalInfo {

    public static ItemStackSnapshot lensItem;
    public static ItemStackSnapshot laserEyeItem;
    public static ItemStackSnapshot metalItem;
    public static ItemStackSnapshot metalCrystalItem;
    public static ItemStackSnapshot metalNuggetItem;
    public static ItemStackSnapshot metalNuggetItemFromIngot;
    public static Text lensName = Text.of(TextColors.RED, "Focusing Lens");
    public static Text laserEyeName = Text.of(TextColors.GREEN, "Laser eye");
    public static Text metalName = Text.of(TextColors.DARK_GRAY, "Scrap Metal");
    public static Text metalCrystalName = Text.of(TextColors.GRAY, "Metal Crystal");
    public static Text metalNuggetName = Text.of(TextColors.GREEN, "Shrapnel");
    public static List<Text> metalLore = Arrays.asList(Text.of(TextColors.GRAY, "Forged from shrapnel collected with a purpose"),
            Text.of(TextColors.DARK_BLUE, "[\u03B2]"));
    public static List<Text> metalNuggetLore = Arrays.asList(Text.of(TextColors.GRAY, "Pieces of metal you found lodged in a zombie's body"),
            Text.of(TextColors.GRAY, "Why are you collecting these???"), CraftingHelper.uncommonLore);
    public static int metalNuggetQuantityPerIngot = 9;

    public MetalInfo()
    {
        checkInitialized();
    }

    private static boolean initialized = false;

    public static void initialize() {
        // these have to be created later so that the builders are defined
        lensItem = ItemStack.builder()
                .itemType(ItemTypes.STAINED_GLASS_PANE)
                .add(Keys.DYE_COLOR, DyeColors.RED)
                .add(Keys.DISPLAY_NAME, lensName)
                .build().createSnapshot();
        laserEyeItem = ItemStack.builder()
                .itemType(ItemTypes.ENDER_EYE)
                .add(Keys.DISPLAY_NAME, laserEyeName)
                .build().createSnapshot();
        metalItem = ItemStack.builder()
                .itemType(ItemTypes.IRON_INGOT)
                .add(Keys.DISPLAY_NAME, metalName)
                // don't add lore here, it is optional
                .build().createSnapshot();
        metalCrystalItem = ItemStack.builder()
                .itemType(ItemTypes.END_CRYSTAL)
                .add(Keys.DISPLAY_NAME, metalCrystalName)
                .build().createSnapshot();
        metalNuggetItem = ItemStack.builder()
                .itemType(ItemTypes.IRON_NUGGET)
                .add(Keys.DISPLAY_NAME, metalNuggetName)
                .build().createSnapshot();
        ItemStack metalNuggetFromIngot = ItemStack.builder()
                .itemType(ItemTypes.IRON_NUGGET)
                .add(Keys.DISPLAY_NAME, metalNuggetName)
                // don't add lore here, it is optional
                .build();
        metalNuggetFromIngot.setQuantity(metalNuggetQuantityPerIngot);
        metalNuggetItemFromIngot = metalNuggetFromIngot.createSnapshot();

        initialized = true;
    }

    public static void checkInitialized() {
        if (!initialized)
        {
            initialize();
        }
    }

}
