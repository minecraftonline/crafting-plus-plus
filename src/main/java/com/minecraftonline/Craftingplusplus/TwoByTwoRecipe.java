package com.minecraftonline.Craftingplusplus;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;

public abstract class TwoByTwoRecipe {

    // org.spongepowered.api.item.inventory.property.SlotPos seems broken, using own Pos instead
    private class Pos
    {
        private final int x;
        private final int y;

        Pos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }
    }

    private HashMap<Pos, Pos[]> squareSlots = new HashMap<Pos, Pos[]>();
    {
        // map of corner positions in grid to spots which must also contain item to form 2x2 square
        // top left corner is 0 0
        // the middle slot (1 1) is assumed to always be required
        squareSlots.put(new Pos(0, 0), new Pos[]{new Pos(0, 1), new Pos(1, 0)});
        squareSlots.put(new Pos(0, 2), new Pos[]{new Pos(0, 1), new Pos(1, 2)});
        squareSlots.put(new Pos(2, 0), new Pos[]{new Pos(1, 0), new Pos(2, 1)});
        squareSlots.put(new Pos(2, 2), new Pos[]{new Pos(1, 2), new Pos(2, 1)});
    }

    public boolean isTwoByTwo(CraftingGridInventory grid) {

        // the middle slot always has to have the item
        Optional<Slot> middleSlot = grid.getSlot(1, 1);
        if (!hasRequiredItem(middleSlot)) return false;

        // there should only be four items
        if (CraftingHelper.getItemCount(grid) != 4) return false;

        for (Map.Entry<Pos, Pos[]> entry : squareSlots.entrySet())
        {
            Pos cornerPos = entry.getKey();

            Optional<Slot> slot = grid.getSlot(cornerPos.getX(), cornerPos.getY());
            if (slot.isPresent())
            {
                if(hasRequiredItem(slot))
                {
                    Pos[] otherPositions = entry.getValue();
                    if(hasRequiredItem(grid.getSlot(otherPositions[0].getX(), otherPositions[0].getY())) &&
                       hasRequiredItem(grid.getSlot(otherPositions[1].getX(), otherPositions[1].getY())))
                    {
                        // return here if valid, no need to continue
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected abstract boolean hasRequiredItem(Optional<Slot> slot);

}
