package com.minecraftonline.Craftingplusplus;

public class TokenNuggetsRecipe extends NuggetsRecipe {

    @Override
    public String getId() {
        return "token_nuggets_from_ingot";
    }

    @Override
    public String getName() {
        return "token nuggets from ingot";
    }

    @Override
    protected BlockIngotNuggetInfo getInfo() {
        return TokenInfo.getInstance();
    }

}
